# Cocktail
Cocktail is the backend part of an MVP for a job.

### Services
##### Postgresql as database:
- Implemented the default and standard settings of Postgresql.
- All queries and models are sent by Django ORM.

##### Django migrations:
- When database models need to be changed or in start up, we trigger this service.
- All models are created and all default values like three tables are created in database when triggered.

##### Django as web backend:
- Used Django Rest framework.
- Table serializing logic and complexity not put in models and is handled when serializing.



### Prerequisites

- docker
- docker-compose

### How to run

1. Create a __.env__ file in project root directory based on __.env.example__ file:

2. Run commands to start django server:
```
docker-compose up --build db
docker-compose up --build migrate
docker-compose up --build web
```

### Run tests
```
docker-compose exec web python manage.py test
```