from django.contrib import admin
from core import models as core_models

admin.site.register(core_models.Cocktail)
admin.site.register(core_models.Ingredient)
