from rest_framework.urls import path
from core import views

urlpatterns = [
    path('add-water/', views.AddWaterView.as_view()),
    path('cocktails/', views.CocktailListAPIView.as_view())
]
