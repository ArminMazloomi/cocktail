from django.db import models


class Cocktail(models.Model):
    label = models.CharField(max_length=50)

    def __str__(self):
        return self.label


class Ingredient(models.Model):
    name = models.CharField(max_length=25)
    volume = models.PositiveIntegerField(default=0)
    cocktail = models.ForeignKey(Cocktail, on_delete=models.CASCADE, related_name='ingredients')

    def __str__(self):
        return self.name
